const chai = require('chai');
const{ assert, expect}= require('chai');
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', (done) => {
		chai.request('http://localhost:5001').get('/forex/getRates')
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/forex/rates')
		.end((err, res) => {
			assert.equal(res.status,200)
			done();	
		})		
	})
	
})


describe ('Currency test suite', () => {

	it("Test api post currency is running", (done) => {
        chai.request("http://localhost:5001")
        .post("/exchangeRates/currency")
        .type("json")
        .send({
            alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
        })
        .end((err,res) => {
            assert.equal(res.status,404)
        })
		done();
    })

	it('test_api_post_currency_returns_200_if_complete_input_given', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)
			
		})
		done();
	})
	
	it('test_api_post_currency_returns_400_if_name_is_missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			// name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status, 400)
			
		});
	
		done();
	})


	it('Test api post currency if name is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 35,
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			
		})
	
		done();
	})

	it('Test api post currency returns an error if name is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: "",
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)
			
		})
	
		done();
	})


	it('Test api post currency returns an error ex is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen'
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})

	it('Test api post currency returns an error ex is not an object', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: 'peso'
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})

	it('Test api post currency returns an error ex is an empty object', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {}
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})


	it('Test api post currency returns an error if alias is missing', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})

	it('Test api post currency returns an error if alias is not a string', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 55,
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})

	it('Test api post currency returns an error if alias is an empty string', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: "",
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})


	it('Test api post currency returns an error if all fields are complete but has a duplicate alias', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			alias: 'peso',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,400)	
		})
		done();
	})


	it('Test api post currency returns 200 if all fields are complete and there are no duplicates', (done) => {
		chai.request('http://localhost:5001')
		.post('/exchangeRates/currency')
		.type('json')
		.send({
			alias: 'nuyen',
			name: 'Shadowrun Nuyen',
			ex: {
				'peso': 0.47,
				'usd': 0.0092,
				'won': 10.93,
				'yuan': 0.065
			}
		})
		.end((err, res) => {
			assert.equal(res.status,200)	
		})
		done();
	})

})



