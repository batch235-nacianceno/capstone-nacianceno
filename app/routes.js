const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	
	router.post('/currency', (req, res) => {

		// returns error if incomplete input
		if(!req.body.hasOwnProperty("alias") || !req.body.hasOwnProperty("name") || !req.body.hasOwnProperty("ex")) {
			return res.status(200).send("Error incomplete parameters")
		} 

		
		// errror if name is missing
		if(!req.body.hasOwnProperty("name")){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter NAME'
			})
		}

		// error if name is not a string
	
		if(typeof req.body.name !== "string"){
			return res.status(400).send({
				'error' : 'Bad Request: name is NOT A STRING'
			})
		}

		// error if name is an empty string

		if(req.body.name.length <= 0){
			return res.status(400).send({
				'error' : 'Bad Request: name is is EMPTY'
			})
		}

		// error is if ex is missing
		if(!req.body.hasOwnProperty("ex")) {
			return res.status(400).send({
				"error": 'Bad Request - missing required parameter EX'
			})
		}

		// error if ex is not an object
		if(typeof req.body.ex !== "object"){
			return res.status(400).send({
				'error' : 'Bad Request: name is NOT AN OBJECT'
			})
		}
		
		// error if ex is an empty object
		if(Object.keys(req.body.ex).length === 0){
			return res.status(400).send({
				'error' : 'Bad Request: EX is an empty object'
			})
		}

		// error if alias is missing

		if(!req.body.hasOwnProperty("alias")) {
			return res.status(400).send({
				"error": 'Bad Request - missing required parameter ALIAS'
			})
		}

		// error if alias is not a string

		if(typeof req.body.alias !== "string") {
			return res.status(400).send({
				"error": 'Bad Request - ALIAS is not a string'
			})
		}

		// error if alias is an empty string

		if(req.body.alias.length <= 0) {
			return res.status(400).send({
				"error": 'Bad Request - ALIAS is an empty string'
			})
		}

		// error if complete fields but duplicate alias

		if(req.body.hasOwnProperty("alias") && req.body.hasOwnProperty("name") && req.body.hasOwnProperty("ex") && req.body.hasOwnProperty("alias")) {
			return res.status(400).send({
				"error": 'Bad Request - ALIAS has a duplicate'
			})
		}

		// check if all fields are complete and no duplicates

		if(req.body.hasOwnProperty("alias") && req.body.hasOwnProperty("name") && req.body.hasOwnProperty("ex")) {
			return res.status(200).send(req.body)
		}
	})




module.exports = router;
